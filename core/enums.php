<?php
namespace prep_state;
interface user_content
{
	const UTF_8 = 'SET NAMES utf8';
	const POPULAR_CONTENT = 'SELECT * FROM content_table ORDER BY DESC LIMIT ?,?';
	const NEW_CONTENT = 'SELECT * FROM content ORDER BY `id` DESC LIMIT ?,?';
	// const RANDOM_CONTENT ='SELECT * FROM content WHERE entry=?' ;
	const CURRENT_ID = 'SELECT id FROM content WHERE id=?';
	const CURRENT_CONTENT = 'SELECT * FROM content WHERE id=?';
	const VOTE_CHECK = 'SELECT * FROM content_votes WHERE content_id=? and ip=?';
	const GET_TIME_VOTE = 'SELECT time FROM content_votes WHERE content_id=? and ip=?';
	const GET_TIME_COMMENT = 'SELECT time FROM comments WHERE content_id=? and ip=?';
	// select * from table order by desc limit a,b where ;
	const GET_COMMENTS_FOR_CURR = 'SELECT * FROM comments WHERE content_id=? ORDER BY `raiting` DESC LIMIT ?,?';
}
interface insert_statement
{
	const INSERT_CONTENT = 'INSERT INTO content(content,raiting,approved) VALUES (?,?,?)';
	const INSERT_VOTES = 'INSERT INTO content_votes (time, content_id, ip) VALUES(?,?,?)';
	const INSERT_COMMENT = 'INSERT INTO comments (content_id,comment_text,raiting,ip,time) VALUES (?,?,?,?,?)';
}
interface update_statement
{
	const UPDATE_RAITING = 'UPDATE content SET raiting=raiting+? where id=?';
	// $values = array($time, $id, $ip);
	const UPDATE_VOTE = 'UPDATE content_votes SET time=? where content_id=? and ip=?';
}
namespace other;
interface defines
{
	const MAX_DISPLAY_PAGES = 20;
}
?>